FROM ruby:2.7.1-slim-buster

SHELL ["/bin/bash", "-c"]

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

### Update, Curl, NodeJS ###
RUN  apt-get -y update \
  && apt-get install -y curl \
  ### PG gem dependency $$$
  && apt-get install -y libpq-dev \
  && apt-get install -y libmagickwand-dev libjpeg-dev libpng-dev libtiff-dev libwebp-dev \
  ### Nokogiri gem dependencies ###
  && apt-get install -y build-essential patch ruby-dev zlib1g-dev liblzma-dev \
  && apt-get install -y libxml2-dev libxslt-dev \
  && curl -sL https://deb.nodesource.com/setup_12.x | /bin/bash - \
  && apt-get install -y nodejs \
  ### Yarn ###
  && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get install -y yarn \
  ### Other deps ###
  && apt-get install -y rsync \
  ### Cleanup ###
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

VOLUME /var/www/att/log /var/www/att/public /var/www/att/tmp

WORKDIR /var/www/att

COPY Gemfile .
COPY Gemfile.lock .

RUN  bundle install

COPY package.json .
COPY yarn.lock .

RUN  yarn install

COPY ./ .

CMD bash entrypoint.sh
