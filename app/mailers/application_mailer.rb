# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'mailer@at-t.ru'
  layout 'mailer'
end
