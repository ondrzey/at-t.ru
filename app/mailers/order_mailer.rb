# frozen_string_literal: true

class OrderMailer < ApplicationMailer
  default from: 'mailer@at-t.ru'

  def send_email(order)
    @order = order
    mail(to: 'zakaz@at-t.ru', subject: "#{@order.id}#Новый заказ с сайта AT-T.ru")
  end

  def phone_order(params)
    @params = params
    mail(to: 'zakaz@at-t.ru', subject: "#{@params[:phone]}#Заказать машину")
  end
end
