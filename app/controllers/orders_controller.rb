# frozen_string_literal: true

class OrdersController < ApplicationController
  before_action :set_order, only: %i[show]

  # GET /orders/new
  def new
    @order = Order.new
  end

  def show
    @text = 'Точную цену для вашего запроса уточняйте у менеджера'
  end

  def create
    if @text.blank?
      if order_params[:weight].to_i <= 600
        lightwieght(order_params)
      elsif order_params[:weight].to_i <= 2000 && order_params[:volume].to_i <= 15
        heavywieght(order_params)
      end
    end
    @order = Order.new(order_params)
    @order.price = @price
    @order.car_enabled = if @car
                           true
                         else
                           false
                         end
    if @order.save
      redirect_to order_path(@order)
    else
      redirect_to root_path
    end
  end

  def send_mail
    @order = Order.find(params[:order_id])
    OrderMailer.send_email(@order).deliver
    flash[:notice] = 'Наш менеджер перезвонит вам через 10 минут!'
    redirect_to root_path
  end

  def modal
    @phone = ''
    respond_to do |format|
      format.html
      format.js
    end
  end

  def phone_order
    OrderMailer.phone_order(params).deliver
    flash[:notice] = 'Наш менеджер перезвонит вам через 10 минут!'
    redirect_to root_path, notice: 'Наш менеджер перезвонит вам через 10 минут!'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    pallet(params) if params[:order][:pallett]
    if session[:pallet].nil?
      if params[:order][:height] && params[:order][:weight] && params[:order][:length]
        sum_volume(params)
      end
      if params[:order][:to].eql?(params[:order][:from]) || params[:order][:to].eql?('Другой город') || params[:order][:from].eql?('Другой город')
        intown(params)
      end
      car(params) if params[:order][:car].present?
    end
    session[:pallet] = nil
    params.require(:order).permit(:name, :surname, :from, :to, :email, :phone, :weight, :volume, :date)
  end

  def pallet(params)
    params[:order][:volume] = (params[:order][:pallett].to_i * 2).to_s
    @price = 4000 if params[:order][:weight].to_i < 100
    session[:pallet] = true
  end

  def sum_volume(params)
    params[:order][:volume] = (params[:order][:height].to_f * params[:order][:width].to_f * params[:order][:length].to_f).to_i.round.to_s
  end

  def intown(_params)
    @price = 0
    @text = 'Точную цену для вашего запроса уточняйте у менеджера'
  end

  def car(params)
    case params[:order][:car]
    when 'Машина до 2 тонн, максимум 16 метров кубических'
      params[:order][:weight] = 2000
      params[:order][:volume] = 16
      @price = 20_000
    when 'Машина до 3 тонн, максимум 20 метров кубических'
      params[:order][:weight] = 3000
      params[:order][:volume] = 20
      @price = 24_000
    when 'Машина до 5 тонн, максимум 35 метров кубических'
      params[:order][:weight] = 5000
      params[:order][:volume] = 35
      @price = 27_000
    end
    @car = true
  end

  def lightwieght(order_params)
    @price = if order_params[:weight].to_i == 100 && order_params[:volume].to_i == 1
               4000
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 1
               4300
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 1
               4700
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 1
               5000
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 1
               5300
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 1
               5800
             elsif order_params[:weight].to_i == 100 && order_params[:volume].to_i == 2
               4300
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 2
               4700
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 2
               5000
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 2
               5300
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 2
               5800
             elsif order_params[:weight].to_i == 600 && order_params[:volume].to_i == 2
               6300
             elsif order_params[:weight].to_i == 100 && order_params[:volume].to_i == 3
               4800
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 3
               5100
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 3
               5500
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 3
               5800
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 3
               6200
             elsif order_params[:weight].to_i == 600 && order_params[:volume].to_i == 3
               6600
             elsif order_params[:weight].to_i == 100 && order_params[:volume].to_i == 4
               5700
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 4
               6000
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 4
               6400
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 4
               6800
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 4
               7100
             elsif order_params[:weight].to_i == 600 && order_params[:volume].to_i == 4
               7500
             elsif order_params[:weight].to_i == 100 && order_params[:volume].to_i == 5
               6500
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 5
               6900
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 5
               7300
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 5
               7800
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 5
               8100
             elsif order_params[:weight].to_i == 600 && order_params[:volume].to_i == 5
               8400
             elsif order_params[:weight].to_i == 100 && order_params[:volume].to_i == 6
               7000
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 6
               7700
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 6
               8000
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 6
               8300
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 6
               8700
             elsif order_params[:weight].to_i == 600 && order_params[:volume].to_i == 6
               9000
             elsif order_params[:weight].to_i == 100 && order_params[:volume].to_i == 7
               7500
             elsif order_params[:weight].to_i == 200 && order_params[:volume].to_i == 7
               8000
             elsif order_params[:weight].to_i == 300 && order_params[:volume].to_i == 7
               8400
             elsif order_params[:weight].to_i == 400 && order_params[:volume].to_i == 7
               8800
             elsif order_params[:weight].to_i == 500 && order_params[:volume].to_i == 7
               9200
             elsif order_params[:weight].to_i == 600 && order_params[:volume].to_i == 7
               9500
             else
               0
             end
    if @price == 0
      @text = 'Точную цену для вашего запроса уточняйте у менеджера'
    end
    end

  def heavywieght(order_params)
    weight = order_params[:weight].to_i
    volume = order_params[:volume].to_i
    @price = if Range.new(700, 899).include?(weight) && Range.new(1, 2).include?(volume)
               6000
             elsif Range.new(700, 899).include?(weight) && Range.new(3, 4).include?(volume)
               7500
             elsif Range.new(700, 899).include?(weight) && Range.new(5, 6).include?(volume)
               8000
             elsif Range.new(700, 899).include?(weight) && Range.new(7, 8).include?(volume)
               9500
             elsif Range.new(700, 899).include?(weight) && Range.new(9, 10).include?(volume)
               10_000
             elsif Range.new(700, 899).include?(weight) && Range.new(10, 12).include?(volume)
               11_000
             elsif Range.new(700, 899).include?(weight) && Range.new(12, 14).include?(volume)
               12_000
             elsif Range.new(700, 899).include?(weight) && Range.new(15, 16).include?(volume)
               13_000
             elsif Range.new(900, 1099).include?(weight) && Range.new(1, 2).include?(volume)
               7500
             elsif Range.new(900, 1099).include?(weight) && Range.new(3, 4).include?(volume)
               8000
             elsif Range.new(900, 1099).include?(weight) && Range.new(5, 6).include?(volume)
               9500
             elsif Range.new(900, 1099).include?(weight) && Range.new(7, 8).include?(volume)
               10_000
             elsif Range.new(900, 1099).include?(weight) && Range.new(9, 10).include?(volume)
               11_000
             elsif Range.new(900, 1099).include?(weight) && Range.new(11, 12).include?(volume)
               11_000
             elsif Range.new(900, 1099).include?(weight) && Range.new(13, 14).include?(volume)
               12_000
             elsif Range.new(900, 1099).include?(weight) && Range.new(15, 16).include?(volume)
               13_000
             elsif Range.new(1100, 1299).include?(weight) && Range.new(1, 2).include?(volume)
               7500
             elsif Range.new(1100, 1299).include?(weight) && Range.new(3, 4).include?(volume)
               8000
             elsif Range.new(1100, 1299).include?(weight) && Range.new(5, 6).include?(volume)
               9500
             elsif Range.new(1100, 1299).include?(weight) && Range.new(7, 8).include?(volume)
               10_000
             elsif Range.new(1100, 1299).include?(weight) && Range.new(9, 10).include?(volume)
               11_000
             elsif Range.new(1100, 1299).include?(weight) && Range.new(11, 12).include?(volume)
               12_000
             elsif Range.new(1100, 1299).include?(weight) && Range.new(13, 14).include?(volume)
               13_000
             elsif Range.new(1100, 1299).include?(weight) && Range.new(15, 16).include?(volume)
               8000
             elsif Range.new(1300, 1499).include?(weight) && Range.new(1, 2).include?(volume)
               9500
             elsif Range.new(1300, 1499).include?(weight) && Range.new(3, 4).include?(volume)
               10_000
             elsif Range.new(1300, 1499).include?(weight) && Range.new(5, 6).include?(volume)
               11_000
             elsif Range.new(1300, 1499).include?(weight) && Range.new(7, 8).include?(volume)
               12_000
             elsif Range.new(1300, 1499).include?(weight) && Range.new(9, 10).include?(volume)
               13_000
             elsif Range.new(1300, 1499).include?(weight) && Range.new(11, 12).include?(volume)
               9500
             elsif Range.new(1300, 1499).include?(weight) && Range.new(13, 14).include?(volume)
               10_000
             elsif Range.new(1300, 1499).include?(weight) && Range.new(15, 16).include?(volume)
               11_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(1, 2).include?(volume)
               12_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(3, 4).include?(volume)
               13_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(5, 6).include?(volume)
               11_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(7, 8).include?(volume)
               12_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(9, 10).include?(volume)
               13_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(11, 12).include?(volume)
               13_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(13, 14).include?(volume)
               11_000
             elsif Range.new(1500, 1699).include?(weight) && Range.new(15, 16).include?(volume)
               12_000
             elsif Range.new(1700, 1999).include?(weight) && Range.new(1, 2).include?(volume)
               13_000
             elsif Range.new(1700, 1999).include?(weight) && Range.new(3, 4).include?(volume)
               11_000
             elsif Range.new(1700, 1999).include?(weight) && Range.new(5, 6).include?(volume)
               12_000
             elsif Range.new(1700, 1999).include?(weight) && Range.new(7, 8).include?(volume)
               13_000
             elsif Range.new(1700, 1999).include?(weight) && Range.new(9, 10).include?(volume)
               13_000
             elsif Range.new(1700, 1900).include?(weight) && Range.new(11, 12).include?(volume)
               11_000
             elsif Range.new(1700, 1900).include?(weight) && Range.new(13, 14).include?(volume)
               12_000
             elsif Range.new(1700, 1900).include?(weight) && Range.new(15, 16).include?(volume)
               13_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(1, 2).include?(volume)
               11_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(3, 4).include?(volume)
               12_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(5, 6).include?(volume)
               13_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(7, 8).include?(volume)
               13_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(9, 10).include?(volume)
               12_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(11, 12).include?(volume)
               13_000
             elsif Range.new(2000, 2300).include?(weight) && Range.new(13, 14).include?(volume)
               13_000
             elsif Range.new(2000, 2300).include?(weight) && volume == 15
               13_000
             else
               0
             end
    if @price == 0
      @text = 'Точную цену для вашего запроса уточняйте у менеджера'
    end
  end
end
