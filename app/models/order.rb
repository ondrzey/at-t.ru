# frozen_string_literal: true

class Order < ApplicationRecord
  validates :phone, presence: true
  validates_format_of :email,
                      with: /\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :from, presence: true
  validates :to, presence: true
end
