# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :surname
      t.string :phone
      t.string :email
      t.string :from
      t.string :to
      t.string :weight
      t.string :volume
      t.integer :price
      t.date :date
      t.boolean :car_enabled

      t.timestamps
    end
  end
end
