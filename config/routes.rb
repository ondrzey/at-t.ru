# frozen_string_literal: true

Rails.application.routes.draw do
  get '/' => 'orders#new'
  get 'modal' => 'orders#modal', as: :modal
  post 'phone_order' => 'orders#phone_order', as: :phone_order
  resources :orders do
    post 'send_mail'
  end
  root to: 'orders#new'
end
