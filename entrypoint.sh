#!/bin/sh
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /var/www/at-t/tmp/pids/server.pid
RAILS_ENV=production rails db:create
RAILS_ENV=production rails db:migrate
puma -e production -b tcp://0.0.0.0:3001
